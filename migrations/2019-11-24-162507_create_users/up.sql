CREATE TABLE users (
    id SERIAL
        PRIMARY KEY,
    created_on TIMESTAMP(2) WITH TIME ZONE
        NOT NULL
        DEFAULT CURRENT_TIMESTAMP(2),
    created_by INTEGER
        REFERENCES users
            ON DELETE RESTRICT,
    username VARCHAR(15)
        UNIQUE
        NOT NULL,
    password TEXT
        NOT NULL,
    active BOOLEAN
        NOT NULL
        DEFAULT TRUE
);

/* Default User admin:admin */
INSERT INTO users(username, password) VALUES('admin', '$argon2i$v=19$m=4096,t=3,p=1$7z9C7XYm2Xya/TMtjQGfy1/it5sYWKqgS5Gm7Y2YFxA$LlmQvHLZ8ppO4xMSy04qabq3cWrIPHqLPAunHIdvs0Q');
