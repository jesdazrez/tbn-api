use diesel::PgConnection;
use rocket_contrib::{database, databases::diesel};

#[cfg(not(test))]
#[database("general")]
pub struct DBConn(PgConnection);

#[cfg(test)]
#[database("test")]
pub struct DBConn(PgConnection);
