table! {
    users (id) {
        id -> Int4,
        created_on -> Timestamptz,
        created_by -> Nullable<Int4>,
        username -> Varchar,
        password -> Text,
        active -> Bool,
    }
}
