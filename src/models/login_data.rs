use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize)]
pub struct LoginData<'a> {
    pub username: &'a str,
    pub password: &'a str,
}
