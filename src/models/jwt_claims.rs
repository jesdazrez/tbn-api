use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize)]
pub struct JWTClaims {
    pub sub: String,
    pub exp: u64,
}
