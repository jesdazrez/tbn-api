use crate::{models::LoginData, schema::users};

use chrono::{DateTime, Utc};
use rand::Rng;

#[derive(Identifiable, Queryable)]
pub struct User {
    pub id: i32,
    pub created_on: DateTime<Utc>,
    pub created_by: Option<i32>,
    pub username: String,
    pub password: String,
    pub active: bool,
}

#[derive(Insertable)]
#[table_name = "users"]
pub struct NewUser<'a> {
    pub created_by: Option<i32>,
    pub username: &'a str,
    pub password: String,
    pub active: bool,
}

impl<'a> NewUser<'a> {
    pub fn new(
        creator_id: Option<i32>,
        credentials: &LoginData<'a>,
    ) -> argon2::Result<NewUser<'a>> {
        let salt: [u8; 32] = rand::thread_rng().gen();
        Ok(NewUser {
            created_by: creator_id,
            username: credentials.username,
            password: argon2::hash_encoded(
                credentials.password.as_bytes(),
                &salt,
                &argon2::Config::default(),
            )?,
            active: true,
        })
    }
}
