pub const JWT_COOKIE_NAME: &str = "__Host-AuthToken";

pub struct JWTSecret(pub Vec<u8>);
pub struct JWTLifetime(pub u64);
