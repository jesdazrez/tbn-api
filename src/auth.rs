use crate::config::{JWTSecret, JWT_COOKIE_NAME};
use crate::models::JWTClaims;

use jsonwebtoken::{decode, Validation};
use rocket::{
    fairing::{Fairing, Info, Kind},
    http::Status,
    Data,
    Outcome::Success,
    Request, Response, State,
};

pub struct Auth;

struct IsAuth(bool);

impl Fairing for Auth {
    fn info(&self) -> Info {
        Info {
            name: "Auth System",
            kind: Kind::Request | Kind::Response,
        }
    }

    fn on_request(&self, request: &mut Request, _: &Data) {
        request.local_cache(|| {
            IsAuth(
                request.uri().path() == "/login"
                    || match request.guard::<State<JWTSecret>>() {
                        Success(jwt_secret) => match request.cookies().get(JWT_COOKIE_NAME) {
                            Some(auth_cookie) => decode::<JWTClaims>(
                                &auth_cookie.value(),
                                &jwt_secret.0,
                                &Validation::default(),
                            )
                            .is_ok(),
                            None => false,
                        },
                        _ => false,
                    },
            )
        });
    }

    fn on_response(&self, request: &Request, response: &mut Response) {
        if !request.local_cache(|| IsAuth(false)).0 {
            response.set_status(Status::Unauthorized);
            response.take_body();
            response.remove_header("Set-Cookie");
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::{setup::instance, tests_setup};

    use std::error::Error;

    use rocket::{http::Status, local::Client};

    #[test]
    fn unauthorized_greeting() -> Result<(), Box<dyn Error>> {
        let client = Client::new(instance())?;
        let mut response = client.get("/hello").dispatch();
        assert_eq!(response.status(), Status::Unauthorized);
        assert!(response.body().is_none());
        Ok(())
    }

    #[test]
    fn authorized_greeting() -> Result<(), Box<dyn Error>> {
        let client = Client::new(instance())?;
        let login_response = client
            .post("/login")
            .body(serde_json::to_string(
                &tests_setup::get_test_user_credentials(client.rocket()),
            )?)
            .dispatch();
        assert_eq!(login_response.status(), Status::Ok);
        let mut greeting_response = client.get("/hello").dispatch();
        assert_eq!(greeting_response.status(), Status::Ok);
        assert!(greeting_response.body().is_some());
        Ok(())
    }
}
