#![feature(proc_macro_hygiene, decl_macro)]
#![feature(once_is_completed)]

mod auth;
mod config;
mod db;
mod models;
mod routes;
mod schema;
mod setup;
#[cfg(test)]
mod tests_setup;

#[macro_use]
extern crate diesel;

#[macro_use]
extern crate diesel_migrations;

// Embed DB migrations in the executable
embed_migrations!("migrations");

fn main() {
    setup::instance().launch();
}
