use rocket::get;

#[get("/hello")]
pub fn hello() -> &'static str {
    "Hello world!"
}
