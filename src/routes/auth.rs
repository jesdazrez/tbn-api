use crate::config::{JWTLifetime, JWTSecret, JWT_COOKIE_NAME};
use crate::db::DBConn;
use crate::models::{JWTClaims, LoginData, User};
use crate::schema::users::dsl::*;

use std::{convert::TryInto, time::SystemTime};

use diesel::{prelude::*, result::Error};
use jsonwebtoken::{encode, Header};
use rocket::{
    http::{Cookie, Cookies, SameSite, Status},
    post, State,
};
use rocket_contrib::json::Json;
use time::Duration;

#[post("/login", data = "<creds>")]
pub fn login(
    conn: DBConn,
    jwt_lifetime: State<JWTLifetime>,
    jwt_secret: State<JWTSecret>,
    creds: Json<LoginData>,
    mut cookies: Cookies,
) -> Result<(), Status> {
    let user = users
        .filter(username.eq(creds.username).and(active.eq(true)))
        .first::<User>(&*conn)
        .map_err(|err| match err {
            Error::NotFound => Status::Unauthorized,
            _ => Status::InternalServerError,
        })?;

    if argon2::verify_encoded(&user.password, creds.password.as_bytes())
        .map_err(|_| Status::InternalServerError)?
    {
        let claims = JWTClaims {
            sub: creds.username.to_owned(),
            exp: SystemTime::now()
                .duration_since(SystemTime::UNIX_EPOCH)
                .map_err(|_| Status::InternalServerError)?
                .as_secs()
                + jwt_lifetime.0,
        };
        let jwt = encode(&Header::default(), &claims, &jwt_secret.0)
            .map_err(|_| Status::InternalServerError)?;
        cookies.add(
            Cookie::build(JWT_COOKIE_NAME, jwt)
                .http_only(true)
                .max_age(Duration::seconds(
                    jwt_lifetime
                        .0
                        .try_into()
                        .map_err(|_| Status::InternalServerError)?,
                ))
                .path("/")
                .same_site(SameSite::Strict)
                .secure(true)
                .finish(),
        );
        return Ok(());
    }

    Err(Status::Unauthorized)
}

#[post("/logout")]
pub fn logout(mut cookies: Cookies) -> Result<(), Status> {
    cookies.remove(
        Cookie::build(JWT_COOKIE_NAME, "")
            .http_only(true)
            .path("/")
            .same_site(SameSite::Strict)
            .secure(true)
            .finish(),
    );
    Ok(())
}

#[cfg(test)]
mod tests {
    use crate::{config::JWT_COOKIE_NAME, models::LoginData, setup::instance, tests_setup};

    use std::error::Error;

    use rocket::{
        http::{Cookie, SameSite, Status},
        local::Client,
    };
    use time::Duration;

    #[test]
    fn login_bad_request() -> Result<(), Box<dyn Error>> {
        let client = Client::new(instance())?;
        let response = client.post("/login").dispatch();
        assert_eq!(response.status(), Status::BadRequest);
        assert!(response.cookies().is_empty());
        Ok(())
    }

    #[test]
    fn login_failure() -> Result<(), Box<dyn Error>> {
        let client = Client::new(instance())?;
        let response = client
            .post("/login")
            .body(serde_json::to_string(&LoginData {
                username: "Username",
                password: "Password",
            })?)
            .dispatch();
        assert_eq!(response.status(), Status::Unauthorized);
        assert!(response.cookies().is_empty());
        Ok(())
    }

    #[test]
    fn login_success() -> Result<(), Box<dyn Error>> {
        let client = Client::new(instance())?;
        let response = client
            .post("/login")
            .body(serde_json::to_string(
                &tests_setup::get_test_user_credentials(client.rocket()),
            )?)
            .dispatch();
        let cookies = response.cookies();
        assert_eq!(response.status(), Status::Ok);
        assert_eq!(cookies.len(), 1);
        let cookie = &cookies[0];
        assert_eq!(cookie.name(), JWT_COOKIE_NAME);
        assert!(!cookie.value().is_empty());
        assert_eq!(cookie.http_only(), Some(true));
        assert_eq!(
            cookie.max_age(),
            Some(Duration::seconds(
                client.rocket().config().get_int("jwt_lifetime")?
            ))
        );
        assert_eq!(cookie.path(), Some("/"));
        assert_eq!(cookie.same_site(), Some(SameSite::Strict));
        assert_eq!(cookie.secure(), Some(true));
        Ok(())
    }

    #[test]
    fn logout_prevent_cookie_name_leak_on_unauthorized() -> Result<(), Box<dyn Error>> {
        let client = Client::new(instance())?;
        let response = client
            .post("/logout")
            .cookie(
                Cookie::build(JWT_COOKIE_NAME, "Wrong Value")
                    .http_only(true)
                    .path("/")
                    .same_site(SameSite::Strict)
                    .secure(true)
                    .finish(),
            )
            .dispatch();
        assert_eq!(response.status(), Status::Unauthorized);
        assert!(response.cookies().is_empty());
        Ok(())
    }

    #[test]
    fn logout_failure() -> Result<(), Box<dyn Error>> {
        let client = Client::new(instance())?;
        let response = client.post("/logout").dispatch();
        assert_eq!(response.status(), Status::Unauthorized);
        assert!(response.cookies().is_empty());
        Ok(())
    }

    #[test]
    fn logout_success() -> Result<(), Box<dyn Error>> {
        let client = Client::new(instance())?;
        let login_response = client
            .post("/login")
            .body(serde_json::to_string(
                &tests_setup::get_test_user_credentials(client.rocket()),
            )?)
            .dispatch();
        assert_eq!(login_response.status(), Status::Ok);
        let logout_response = client.post("/logout").dispatch();
        assert_eq!(logout_response.status(), Status::Ok);
        assert!(!logout_response.cookies().is_empty());
        Ok(())
    }
}
