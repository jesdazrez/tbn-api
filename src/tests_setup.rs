use crate::{
    db::DBConn,
    models::{LoginData, NewUser, User},
    schema::users::dsl::*,
};

use std::sync::Once;

use diesel::{insert_into, prelude::*};
use rocket::Rocket;

static TEST_INIT: Once = Once::new();
const USER_CREDENTIALS: LoginData = LoginData {
    username: "Foo",
    password: "Bar",
};
static mut USER_DATA: Option<User> = None;

pub fn get_test_user_credentials(instance: &Rocket) -> LoginData {
    if !TEST_INIT.is_completed() {
        get_test_user(instance);
    }
    USER_CREDENTIALS
}

pub fn get_test_user(instance: &Rocket) -> &User {
    unsafe {
        TEST_INIT.call_once(|| {
            let conn = DBConn::get_one(&instance).unwrap();
            let user = users
                .filter(username.eq(USER_CREDENTIALS.username))
                .first::<User>(&*conn)
                .or_else(|_| {
                    insert_into(users)
                        .values(&NewUser::new(None, &USER_CREDENTIALS).unwrap())
                        .get_result::<User>(&*conn)
                })
                .unwrap();
            USER_DATA = Some(user);
        });
        USER_DATA.as_ref().unwrap()
    }
}
