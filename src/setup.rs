use crate::{
    auth::Auth,
    config::{JWTLifetime, JWTSecret},
    db::DBConn,
    embedded_migrations, routes,
};

use std::convert::TryInto;

use base64::decode;
use rocket::{fairing::AdHoc, routes, Rocket};

pub fn instance() -> Rocket {
    rocket::ignite()
        .attach(DBConn::fairing())
        .attach(AdHoc::on_attach("Read configuration", |rocket| {
            let config = rocket.config();
            let jwt_lifetime = config.get_int("jwt_lifetime").unwrap();
            let jwt_secret = decode(config.get_str("jwt_secret").unwrap()).unwrap();
            Ok(rocket
                .manage(JWTLifetime(jwt_lifetime.try_into().unwrap()))
                .manage(JWTSecret(jwt_secret)))
        }))
        .attach(AdHoc::on_attach(
            "Run DB migrations",
            |rocket| match DBConn::get_one(&rocket) {
                Some(conn) => {
                    match embedded_migrations::run_with_output(&*conn, &mut std::io::stdout()) {
                        Ok(_) => Ok(rocket),
                        Err(_) => Err(rocket),
                    }
                }
                None => Err(rocket),
            },
        ))
        .attach(Auth)
        .mount("/", routes![routes::hello, routes::login, routes::logout])
}
