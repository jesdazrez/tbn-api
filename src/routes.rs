mod auth;
mod hello;

pub use auth::login;
pub use auth::logout;
pub use auth::static_rocket_route_info_for_login;
pub use auth::static_rocket_route_info_for_logout;

pub use hello::hello;
pub use hello::static_rocket_route_info_for_hello;
